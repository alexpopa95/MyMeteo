package com.alex.mymeteo;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.alex.mymeteo.adapters.SearchCityListAdapter;
import com.alex.mymeteo.objects.Placeholder;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView mCityListView;
    private RecyclerView.Adapter mCityListAdapter;
    private RecyclerView.LayoutManager mCityListManager;

    private ProgressBar progressBar;
    private Placeholder placeholder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setTitle("Ricerca");

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        mCityListView = (RecyclerView) findViewById(R.id.search_city_list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        placeholder = new Placeholder(findViewById(R.id.placeholder));
        placeholder.setIcon(R.drawable.favorite);
        placeholder.setText("Aggiungi alcune città preferite.\nCompariranno qui...");

        if(mCityListView != null) mCityListView.setHasFixedSize(true);

        mCityListManager = new LinearLayoutManager(this);
        mCityListView.setLayoutManager(mCityListManager);
        placeholder.setIcon(R.drawable.search);
        placeholder.setText("Cerca una città");
        placeholder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem actionView = menu.findItem(R.id.action_search);
        actionView.expandActionView();
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(actionView);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Scrivi il nome di una città");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mCityListAdapter = new SearchCityListAdapter(SearchActivity.this, new ArrayList<String>(), new ArrayList<String>());
                mCityListView.setAdapter(mCityListAdapter);
                progressBar.setVisibility(View.VISIBLE);
                Api.searchCities(query, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        ArrayList<String> cities = new ArrayList<>();
                        ArrayList<String> citiesAddress = new ArrayList<>();
                        try {
                            for(int i=0;i<response.getJSONArray("results").length();i++) {
                                cities.add(response.getJSONArray("results").getJSONObject(i).getString("name"));
                                citiesAddress.add(response.getJSONArray("results").getJSONObject(i).getString("formatted_address"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(cities.size() == 0) {
                            placeholder.setIcon(R.drawable.sadface);
                            placeholder.setText("Nessuna città trovata");
                            placeholder.show();
                        }
                        else {
                            placeholder.hide();
                        }

                        mCityListAdapter = new SearchCityListAdapter(SearchActivity.this, cities, citiesAddress);
                        mCityListView.setAdapter(mCityListAdapter);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        return super.onOptionsItemSelected(item);
    }
}
