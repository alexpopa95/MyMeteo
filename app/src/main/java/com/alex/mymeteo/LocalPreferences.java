package com.alex.mymeteo;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by alexpopa95 on 23 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */
public class LocalPreferences {

    private static SharedPreferences mSharedPreferences;

    public static SharedPreferences getInstance(Context context) {
        if(mSharedPreferences == null) mSharedPreferences = context.getSharedPreferences("mymeteo_prefs", Context.MODE_PRIVATE);
        return mSharedPreferences;
    }

    public static void saveValue(Context context, String key, String value) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.putString(key, value);
        editor.apply();
    }
}
