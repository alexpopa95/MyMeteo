package com.alex.mymeteo;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.alex.mymeteo.adapters.CityListAdapter;
import com.alex.mymeteo.objects.CityObject;
import com.alex.mymeteo.objects.Placeholder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by alexpopa95 on 23 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */
public class MainActivity extends AppCompatActivity {
    public static String TAG = "MainActivity";

    private RecyclerView mCityListView;
    private RecyclerView.Adapter mCityListAdapter;
    private RecyclerView.LayoutManager mCityListManager;

    private FloatingActionButton searchButton;
    private Placeholder placeholder;                // Placeholder is custom class to easy maange a custom view
                                                    // included in layout xml. <include id='palceholder'>

    private static ArrayList<CityObject> cities;    // CityObject array. For code aknowledge
    private static JSONArray jsonCities;            // jsonArray - Will be saved as string in memory (preferences)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Città salvate");

        mCityListView = (RecyclerView) findViewById(R.id.city_list);
        searchButton = (FloatingActionButton) findViewById(R.id.searchButton);
        placeholder = new Placeholder(findViewById(R.id.placeholder));
        placeholder.setIcon(R.drawable.favorite);
        placeholder.setText("Aggiungi alcune città preferite.\nCompariranno qui...");

        mCityListView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 0)
                    searchButton.hide();
                else if (dy < 0)
                    searchButton.show();
            }
        });
        mCityListView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        if(mCityListView != null) mCityListView.setHasFixedSize(true);

        mCityListManager = new LinearLayoutManager(this);
        mCityListView.setLayoutManager(mCityListManager);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Loads favorite cities from a static variable 'cities'.
        if(cities == null) {
            cities = new ArrayList<>();
            CityObject city;
            try {
                jsonCities = new JSONArray(LocalPreferences.getInstance(this).getString("cities", "[]"));
                for(int i=0;i<jsonCities.length();i++) {
                    city = new CityObject(jsonCities.getJSONObject(i));
                    cities.add(city);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mCityListAdapter = new CityListAdapter(MainActivity.this, cities);
        mCityListView.setAdapter(mCityListAdapter);

        if(cities.size() == 0) placeholder.show();
        else placeholder.hide();
    }

    // Check if city name is in local memoru.
    // If true then it means the city is one of user's favorite city
    public static boolean isFavoriteCity(String name) {
        for(CityObject city : cities) {
            if(city.getName().equals(name)) return true;
        }
        return false;
    }

    // Removes city from local memory. No action if it dowsn't exists
    public static void removeFavoriteCity(Context context, String name) {
        for(int i=0;i<cities.size();i++) {
            if(cities.get(i).getName().equals(name)) {
                cities.remove(i);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    jsonCities.remove(i);
                    LocalPreferences.saveValue(context, "cities", jsonCities.toString());
                }
                else {
                    JSONArray output = new JSONArray();
                    int len = jsonCities.length();
                    for (int j = 0; j < len; j++)   {
                        if (j != i) {
                            try {
                                output.put(jsonCities.get(j));
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                    LocalPreferences.saveValue(context, "cities", output.toString());
                }
                break;
            }
        }
    }

    // Add new city to local memory. If already exists it will be removed and saved again.
    public static void addFavoriteCity(Context context, CityObject city) {
        removeFavoriteCity(context, city.getName());
        cities.add(city);
        jsonCities.put(city.getAsJSON());
        LocalPreferences.saveValue(context, "cities", jsonCities.toString());
    }
}
