package com.alex.mymeteo.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alex.mymeteo.MeteoActivity;
import com.alex.mymeteo.R;
import com.alex.mymeteo.objects.CityObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by alexpopa95 on 22 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */
public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.CityViewHolder> {
    private ArrayList<CityObject> mCities;
    private Activity activity;

    public static class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Activity activity;
        public LinearLayout card;
        public TextView mCityName;
        public TextView mTemperature;
        public TextView mDescription;
        public ImageView mMeteoIcon;
        public ImageView mCityIcon;
        public CityViewHolder(Activity activity, LinearLayout v) {
            super(v);
            this.card = v;
            this.activity = activity;
            this.card.setClickable(true);
            mCityName = (TextView) v.findViewById(R.id.cityName);
            mTemperature = (TextView) v.findViewById(R.id.temperature);
            mDescription = (TextView) v.findViewById(R.id.description);
            mMeteoIcon = (ImageView) v.findViewById(R.id.meteoIcon);
            mCityIcon = (ImageView) v.findViewById(R.id.cityIcon);
            View clickable = ((View) mCityName.getParent().getParent().getParent());
            clickable.setFocusable(true);
            clickable.setClickable(true);
            clickable.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, MeteoActivity.class);
            intent.putExtra("cityName", mCityName.getText());
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    public CityListAdapter(Activity activity, ArrayList<CityObject> myDataset) {
        this.mCities = myDataset;
        this.activity = activity;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.object_city_card, parent, false);
        CityViewHolder vh = new CityViewHolder(activity, v);
        return vh;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        final CityObject city = mCities.get(position);
        if(city != null) {
            holder.mTemperature.setText(String.valueOf(city.getLastTemperature()+"°C"));
            holder.mCityName.setText(city.getName());
            holder.mDescription.setText(city.getLastDescription());
            if(!city.getLastMeteoIconUrl().isEmpty()) Picasso.with(holder.card.getContext()).load(city.getLastMeteoIconUrl()).into(holder.mMeteoIcon);
            city.setOnFinishUpdateListener(new CityObject.OnFinishUpdateListener() {
                @Override
                public void onSuccessUpdate(CityObject city) {
                    notifyDataSetChanged();
                }

                @Override
                public void onFailureUpdate(CityObject city, String message) {}
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCities.size();
    }
}
