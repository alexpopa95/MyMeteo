package com.alex.mymeteo.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alex.mymeteo.MeteoActivity;
import com.alex.mymeteo.R;

import java.util.ArrayList;

/**
 * Created by alexpopa95 on 22 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */
public class SearchCityListAdapter extends RecyclerView.Adapter<SearchCityListAdapter.CityViewHolder> {
    private ArrayList<String> mCities, mCitiesAddress;
    private Activity activity;

    public static class CityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Activity activity;
        public RelativeLayout card;
        public TextView mCityName;
        public TextView mCityAddress;
        public CityViewHolder(Activity activity, RelativeLayout v) {
            super(v);
            this.card = v;
            this.activity = activity;
            this.card.setClickable(true);
            mCityName = (TextView) v.findViewById(R.id.name);
            mCityAddress = (TextView) v.findViewById(R.id.address);
            ((View) mCityName.getParent()).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(activity, MeteoActivity.class);
            intent.putExtra("cityName", mCityName.getText());
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }
    }

    public SearchCityListAdapter(Activity activity, ArrayList<String> myDataset, ArrayList<String> myDatasetAddress) {
        this.mCities = myDataset;
        this.mCitiesAddress = myDatasetAddress;
        this.activity = activity;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.object_search_city_item, parent, false);

        CityViewHolder vh = new CityViewHolder(activity, v);
        return vh;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        holder.mCityName.setText(mCities.get(position));
        holder.mCityAddress.setText(mCitiesAddress.get(position));
    }

    @Override
    public int getItemCount() {
        return mCities.size();
    }
}
