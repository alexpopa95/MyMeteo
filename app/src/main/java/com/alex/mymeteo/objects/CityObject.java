package com.alex.mymeteo.objects;

import com.alex.mymeteo.Api;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by alexpopa95 on 22 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */

public class CityObject {
    private String name = "";
    private Integer lastTemperature = 0;
    private String lastDescription = "";
    private String lastMeteoIconUrl = "";
    private boolean favorite;

    private OnFinishUpdateListener onFinishUpdateListener;

    public CityObject(String name){
        this.name = name;
    }

    public CityObject(JSONObject json){
        try {
            this.name = json.getString("name");
            this.lastTemperature = json.getInt("lastTemperature");
            this.lastDescription = json.getString("lastDescription");
            this.lastMeteoIconUrl = json.getString("lastMeteoIconUrl");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getAsJSON() {
        JSONObject object = new JSONObject();
        try {
            object.put("name", name);
            object.put("lastTemperature", lastTemperature);
            object.put("lastDescription", lastDescription);
            object.put("lastMeteoIconUrl", lastMeteoIconUrl);
            object.put("favorite", favorite);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
        if(favorite) {

        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLastTemperature() {
        return lastTemperature;
    }

    public String getLastDescription() {
        return lastDescription;
    }

    public String getLastMeteoIconUrl() {
        return lastMeteoIconUrl;
    }

    public void setOnFinishUpdateListener(OnFinishUpdateListener onFinishUpdateListener) {
        this.onFinishUpdateListener = onFinishUpdateListener;
    }

    public void updateTemperature() {
        if(name == null) return;
        Api.getMeteoForCity(name, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(response.getInt("is_success") == 1) {
                        lastDescription = response.getString("weatherDesc");
                        lastTemperature = response.getInt("temperature");
                        lastMeteoIconUrl = response.getString("weatherIconUrl");
                        if(onFinishUpdateListener != null) onFinishUpdateListener.onSuccessUpdate(CityObject.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if(onFinishUpdateListener != null) onFinishUpdateListener.onFailureUpdate(CityObject.this, "Could not parse JSON");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if(onFinishUpdateListener != null) onFinishUpdateListener.onFailureUpdate(CityObject.this, "Unknown GET Request error");
            }
        });
    }

    public interface OnFinishUpdateListener {
        void onSuccessUpdate(CityObject city);
        void onFailureUpdate(CityObject city, String message);
    }
}
