package com.alex.mymeteo.objects;

import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.mymeteo.R;

/**
 * Created by alexpopa95 on 23 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo.objects
 */
public class Placeholder {
    private View view;
    private ImageView icon;
    private TextView text;

    /*
    Manages a custom View between various views.
    You pass the view to the class.
    It parses all the information and makes the view an object that can be easily modified.
     */

    public Placeholder(View view) {
        this.view = view;
        if(view != null) {
            text = (TextView) view.findViewById(R.id.text);
            icon = (ImageView) view.findViewById(R.id.icon);
        }
    }

    public void show() {
        view.setVisibility(View.VISIBLE);
    }

    public void hide() {
        view.setVisibility(View.GONE);
    }

    public void setText(@NonNull String text) {
        this.text.setText(text);
    }

    public void setTextColor(@ColorInt int color) {
        this.text.setTextColor(color);
    }

    public void setIcon(@DrawableRes int icon) {
        this.icon.setImageResource(icon);
    }

    public View getView() {
        return view;
    }
}
