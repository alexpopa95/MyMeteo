package com.alex.mymeteo;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by alexpopa95 on 22 Giugno 2016
 * Project: MyMeteo
 * Package: com.alex.mymeteo
 */
public class Api {
    private static AsyncHttpClient client;
    private static final String BASE_URL = "http://test1.yourspaceweb.it";
    private static final String GOOGLE_MAPS_BASE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json";
    private static final String GOOGLE_MAPS_API_KEY = "AIzaSyAM_lpA4_gdIwvQ2dJ4xKI0sesTTtAjSwc";

    public static AsyncHttpClient getClientInstance() {
        if(client == null) client = new AsyncHttpClient();
        return client;
    }

    public static void getMeteoForCity(String city_name, JsonHttpResponseHandler jsonResponseHandler) {
        RequestParams params = new RequestParams();
        params.add("zipcode", city_name);
        params.add("key", "test-DVC");
        params.add("output", "json");
        getClientInstance().get(BASE_URL+"/meteo.php", params, jsonResponseHandler);
    }

    public static void searchCities(String city_name, JsonHttpResponseHandler jsonResponseHandler) {
        RequestParams params = new RequestParams();
        params.add("query", city_name);
        params.add("type", "locality");
        params.add("key", GOOGLE_MAPS_API_KEY);
        getClientInstance().get(GOOGLE_MAPS_BASE_URL, params, jsonResponseHandler);
    }
}
