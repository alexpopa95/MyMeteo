package com.alex.mymeteo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alex.mymeteo.objects.CityObject;
import com.alex.mymeteo.objects.Placeholder;
import com.squareup.picasso.Picasso;

public class MeteoActivity extends AppCompatActivity {

    private CityObject city;
    private Placeholder placeholder;
    private TextView mCityName;
    private TextView mDescription;
    private TextView mTemperature;
    private ImageView mMeteoIcon;

    private FloatingActionButton favoriteButton;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteo);

        actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        mCityName = (TextView) findViewById(R.id.meteoCityName);
        mDescription = (TextView) findViewById(R.id.meteoDescription);
        mTemperature = (TextView) findViewById(R.id.meteoTemperature);
        mMeteoIcon = (ImageView) findViewById(R.id.meteoIcon);
        favoriteButton = (FloatingActionButton) findViewById(R.id.favoriteButton);

        placeholder = new Placeholder(findViewById(R.id.placeholder));
        placeholder.setIcon(R.drawable.sadface);
        placeholder.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        placeholder.setText("C'è stato un errore nella sincronizzazione");
        placeholder.hide();

        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(city.isFavorite()) {
                    MainActivity.removeFavoriteCity(MeteoActivity.this, city.getName());
                }
                else {
                    MainActivity.addFavoriteCity(MeteoActivity.this, city);
                }
                if(!city.isFavorite()) favoriteButton.setImageResource(R.drawable.heart_selected);
                else favoriteButton.setImageResource(R.drawable.heart);
                city.setFavorite(!city.isFavorite());
            }
        });

        Intent intent = getIntent();
        if(intent != null) {
            final String cityName = intent.getStringExtra("cityName");
            setTitle(cityName);
            boolean isFavorite = MainActivity.isFavoriteCity(cityName);
            mCityName.setText(cityName);
            city = new CityObject(cityName);
            city.setFavorite(isFavorite);
            if(isFavorite) favoriteButton.setImageResource(R.drawable.heart_selected);
            else favoriteButton.setImageResource(R.drawable.heart);
            city.setOnFinishUpdateListener(new CityObject.OnFinishUpdateListener() {
                @Override
                public void onSuccessUpdate(CityObject city) {
                    Picasso.with(MeteoActivity.this).load(city.getLastMeteoIconUrl()).into(mMeteoIcon);
                    mTemperature.setText(String.valueOf(city.getLastTemperature()+"°C"));
                    mDescription.setText(city.getLastDescription());
                    setTitle(city.getName());
                    if(actionBar != null) {
                        actionBar.setSubtitle(city.getLastTemperature()+"°C");
                    }
                }

                @Override
                public void onFailureUpdate(CityObject city, String message) {
                    placeholder.show();
                }
            });
            city.updateTemperature();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        return super.onOptionsItemSelected(item);
    }
}
